fifo documentation
--------------------

The docbook source in `fifo.xml` is used to generate the Unix
man-page `fifo.3` and the plain text `fifo.txt`. The tools 
`xsltproc` and `lynx` are required to regenerate these files.

The script `fifo-fetch.sh` pulls the latest version of `fifo.c`
and `fifo.h` from the GitHub master to the current directory.
