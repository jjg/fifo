#!/bin/sh
#
# refresh fifo.* from git master

NAME=fifo
URL="https://gitlab.com/jjg/$NAME/raw/master"

for ext in c h
do
    FILE="$NAME.$ext"
    echo -n "$FILE .."
    wget -q $URL/$FILE -O $FILE
    echo ". done"
done
